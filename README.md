# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

After cloning the project please run:
### `npm install`

In the project directory, you can run:

### `npm start`

I modified the package.json to launch json-server on port 3000 and then the app on 3001. If it doesn't launch by it's self please ctrl + click on the link bellow.

Runs the app in the development mode.\
Open [http://localhost:3001](http://localhost:3001) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

There are a two features that need the refresh of the page to be taken into account like delete and add new tasks.

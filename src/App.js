
import './App.css';
// Import Components
import InputTask from './components/Form';
import ToDoList from './components/ToDoList';

function App() {
  return (
    <div className="App">
      <header>
        <h1 className='text-primary'> To Do List</h1>
        <InputTask />
      </header>
      <main>
        <ToDoList />
      </main>
    </div>
  );
}

export default App;

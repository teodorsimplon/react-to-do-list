import { useState } from "react";
import dateFormat from 'dateformat';
export function Task(props) {

    let {
        id,
        title,
        description,
        date,
        isFinished
    } = props.task;

    const setIsFinished = () => {
        isFinished = !isFinished;

        fetch(`http://localhost:3000/task/${id}`, {

            headers: {
                'Content-Type': 'application/json'
            },
            method: "PATCH",
            body: JSON.stringify({
                'id': id,
                'title': title,
                'description': description,
                'date': date,
                'isFinished': isFinished,
            })
        })
    }

    const deleteTask = () => {
        fetch(`http://localhost:3000/task/${id}`, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "DELETE",
        })
    }

    //Test commit
    
    
    

    return (
        
        <div className="border border-secondary border-3 rounded-3 my-4 shadow p-3" >
            <h4 className={isFinished ? "text-decoration-line-through" : ""}>
                {title}
            </h4>
            <p className={isFinished ? "text-decoration-line-through" : ""}>
                {description}
            </p>
            <time> Created At: {dateFormat(date)}</time>
            <p><input className="form-check-input" type="checkbox" defaultChecked={isFinished} onChange={() => setIsFinished()}></input>Check If Completed </p>
            <button className="btn btn-secondary" onClick={() => deleteTask()}>Delete Task</button>
        </div>
    )
}
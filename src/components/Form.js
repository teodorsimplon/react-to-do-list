import React from "react";

class InputTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        fetch("http://localhost:3000/task", {

            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({
                'title': this.state.title,
                'description': this.state.description,
                'date': new Date(),
                'isFinished': false,
            })

        })
        this.setState({
            title: "",
            description: ""
        })

    }

    render() {
        return (
            <div className="container border border-secondary border-3 rounded-3 my-3 shadow p-3">
                <form >
                    <h2>Add new task</h2>
                    <div className="mb-3">
                        <label className="form-label">Title:</label>
                        <input className="form-control" type="text" value={this.state.title} name='title' onChange={this.handleChange}></input>
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Description: </label>
                        <input className="form-control" type="text" value={this.state.description} name='description' onChange={this.handleChange}></input>
                    </div>
                    <button type="submit" onClick={this.handleSubmit} className="btn btn-secondary">Submit</button>
                </form>

            </div>
        );
    }
}

export default InputTask;
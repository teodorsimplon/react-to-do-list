import React from "react";
import { Task } from "./Task";
class ToDoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: []
        }
    }

    getFetchTasks() {
        fetch('http://localhost:3000/task')
            .then(response => response.json())
            .then(responseJson => {
                console.log("response json", responseJson);
                this.setState({
                    tasks: responseJson
                })
            })
            .catch(err => { console.log(err) })
    }
    componentDidMount() {
        this.getFetchTasks()
    }
    render() {
        return (
            <div className="container">
                {this.state.tasks.reverse().map(task => <Task key={task.id} task={task} />)}
            </div>
        );
    }
}

export default ToDoList;